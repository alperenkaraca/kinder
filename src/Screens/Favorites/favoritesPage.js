import React from 'react'
import { View, FlatList } from 'react-native'
import { connect } from 'react-redux'

import { FavoriteCard, NoData } from '../../components/index'
import { removeFavoriteAction  } from '../../actions/favorite'


class FavoritesPage extends React.Component {
    constructor(props){
        super(props)
    }

    deleteItem = (item) => {
      this.props.removeFavorites(item)
    }

    render(){
      const { allFavorites } = this.props
        return(
            <View style={{ flex:1,}}>
               <FlatList
                   contentContainerStyle={allFavorites.length === 0 && { justifyContent: 'center', alignItems: 'center', height: '100%' }}
                    ListEmptyComponent={<NoData text={'Favori Listeniz Bos'} iconName={'favorite'} />}
                    data={allFavorites}
                    renderItem={({ item }) => <FavoriteCard item={item} deleteItem={this.deleteItem} />}
                    keyExtractor={item => item.fullnameId}
                />
            </View>
        )
    }
}

maptoStatetoProps = (state) => {
  return {
    allFavorites: state.favorites.allFavorites
  }
}

mapDispatchToProps = (dispatch) => {
  return {
    removeFavorites: (item) => dispatch(removeFavoriteAction(item)),
  }
}

export default connect(
  mapStatetoProps,
  mapDispatchToProps
)(FavoritesPage)
