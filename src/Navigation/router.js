import React from 'react'
import { View, Text } from 'react-native'
import { connect } from 'react-redux'
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import HomePage from '../screens/home/homePage'
import FavoritesPage from '../screens/favorites/favoritesPage'

function IconWithBadge({ name, badgeCount, color, size }) {
  return (
    <View style={{ width: 24, height: 24, margin: 5 }}>
      <MaterialIcon name={name} size={size} color={color} />
      {badgeCount > 0 && (
        <View
          style={{
            position: 'absolute',
            right: -6,
            top: -3,
            backgroundColor: 'red',
            borderRadius: 6,
            width: 12,
            height: 12,
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Text style={{ color: 'white', fontSize: 10, fontWeight: 'bold' }}>
            {badgeCount}
          </Text>
        </View>
      )}
    </View>
  );
}

const  FavoriteIconWithBadge = (props) =>{
  return <IconWithBadge {...props} badgeCount={props.allFavorites.length} />;
}

function mapStatetoProps(state) {
  return {
    allFavorites:state.favorites.allFavorites
  }
}

const ConnectedFavoritesIconWithBadge = connect(mapStatetoProps)(FavoriteIconWithBadge) 
const Tab = createBottomTabNavigator()

const Router = () => {
    return(
<NavigationContainer>
<Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size , tintColor}) => {
            let iconName;

            if (route.name === 'Home') {
              iconName = 'home'
            } else {
              return (
                <ConnectedFavoritesIconWithBadge
                name={'favorite'}
                size={25}
                color={color}
              />
              )
             
            }

            // You can return any component that you like here!
            return <MaterialIcon name={iconName} size={size} color={color} />;
          },
        })}
        tabBarOptions={{
          activeTintColor: 'tomato',
          inactiveTintColor: 'gray',
        }}
      >
     
        <Tab.Screen name="Home" component={HomePage} />
        <Tab.Screen name="Favorilerim" component={FavoritesPage} />
      </Tab.Navigator>
</NavigationContainer>
    )
} 


export default Router