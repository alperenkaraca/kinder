import React from 'react'
import { StyleSheet, Text, View, Dimensions, Image, Animated } from 'react-native';

const SCREEN_HEIGHT = Dimensions.get('window').height
const SCREEN_WIDTH = Dimensions.get('window').width

export const Card = ({ item, index }) => {
  return (
    <Animated.View
      key={index}
      style={cardStyle.main}
    >
      <View style={cardStyle.contentContainer}>
        <Image
          style={cardStyle.image}
          source={{ uri: item.url }}
        />
        <View style={cardStyle.footer}>
          <Text style={cardStyle.score}>{item.score}</Text>
          <Text style={cardStyle.title}>{item.title}</Text>
        </View>
      </View>

    </Animated.View>
  )
}

const cardStyle = StyleSheet.create({
  main: {
    height: SCREEN_HEIGHT - 120,
    width: SCREEN_WIDTH,
    padding: 20,
    position: 'absolute'
  },
  contentContainer: { backgroundColor: '#fff', borderRadius: 10, flex: 1, padding: 10 },
  image: {
    flex: 0.9,
    height: null,
    width: null,
    resizeMode: "cover",
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  footer: { flex: 0 },
  score: { color: '#9CD1CF', fontSize: 20, fontWeight: '800', marginVertical: 10, textAlign: 'right' },
  title: { color: '#5F8785', }

})