import React from 'react'
import { StyleSheet, Dimensions, View, Text, PanResponder, Animated, Image } from 'react-native'

import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux'
import { addFavoriteAction } from '../actions/favorite'
import { Card } from './card'
import { Loading } from './loading'

import { hostListQuery } from '../queries/index'
import { Query } from 'react-apollo'

const CardContext = React.createContext({
  cards: []
})
const SCREEN_HEIGHT = Dimensions.get('window').height
const SCREEN_WIDTH = Dimensions.get('window').width
class CardStack extends React.PureComponent {
  static contextType = CardContext
  constructor(props) {
    super(props);
    this.state = {
      cards: this.props.cards,
      currentIndex: 0
    };
    this.createStack()

    this.position = new Animated.ValueXY();
    this.rotate = this.position.x.interpolate({
      inputRange: [-SCREEN_WIDTH / 2, 0, SCREEN_WIDTH / 2],
      outputRange: ['-10deg', '0deg', '10deg'],
      extrapolate: 'clamp'
    })
    this.rotateAndTranslate = {
      transform: [{
        rotate: this.rotate
      },
      ...this.position.getTranslateTransform()
      ]
    }
    this.likeOpacity = this.position.x.interpolate({
      inputRange: [-SCREEN_WIDTH / 2, 0, SCREEN_WIDTH / 2],
      outputRange: [0, 0, 1],
      extrapolate: 'clamp'
    })

    this.nopeOpacity = this.position.x.interpolate({
      inputRange: [-SCREEN_WIDTH / 2, 0, SCREEN_WIDTH / 2],
      outputRange: [1, 0, 0],
      extrapolate: 'clamp'
    })
    this.nextCardOpacity = this.position.x.interpolate({
      inputRange: [-SCREEN_WIDTH / 2, 0, SCREEN_WIDTH / 2],
      outputRange: [1, 0, 1],
      extrapolate: 'clamp'
    })

    this.nextCardScale = this.position.x.interpolate({
      inputRange: [-SCREEN_WIDTH / 2, 0, SCREEN_WIDTH / 2],
      outputRange: [1, 0.8, 1],
      extrapolate: 'clamp'
    })

  }

  createStack = () => {
    this.PanResponder = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onPanResponderMove: (evt, gestureState) => {
        this.position.setValue({ x: gestureState.dx, y: gestureState.dy });
      },
      onPanResponderRelease: (evt, gestureState) => {
        if (gestureState.dx > 120) {
          Animated.spring(this.position, {
            toValue: { x: SCREEN_WIDTH + 100, y: gestureState.dy }
          }).start(() => {
            this.setState({ currentIndex: this.state.currentIndex + 1 }, () => {
              this.position.setValue({ x: 0, y: 0 })
              const addedItem = this.context.cards[this.state.currentIndex]
              this.props.addFavorites(addedItem)
            })
          })
          console.log('Liked', this.state.currentIndex)
        } else if (gestureState.dx < -120) {
          Animated.spring(this.position, {
            toValue: { x: -SCREEN_WIDTH - 100, y: gestureState.dy }
          }).start(() => {
            this.setState({ currentIndex: this.state.currentIndex + 1 }, () => {
              this.position.setValue({ x: 0, y: 0 })

            })
          })
          console.log('UnLiked', this.state.currentIndex)
        } else {
          Animated.spring(this.position, {
            toValue: { x: 0, y: 0 },
            friction: 4
          }).start()
        }
      }
    })
  }

  renderCards = (cards) => {
    return cards.map((item, i) => {
      if (i < this.state.currentIndex) {
        return null;
      } else if (i == this.state.currentIndex) {
        return (
          <Animated.View
            {...this.PanResponder.panHandlers}
            key={i}
            style={[
              this.rotateAndTranslate,
              {
                opacity: this.nextCardOpacity,
                height: SCREEN_HEIGHT - 220,
                width: SCREEN_WIDTH,
                padding: 10,
                position: "absolute"
              }
            ]}
          >
            <Animated.View
              style={{
                opacity: this.likeOpacity,
                transform: [{ rotate: "-30deg" }],
                position: "absolute",
                top: 50,
                left: 40,
                zIndex: 1000
              }}
            >
              <MaterialIcon name={'mood'} size={50} color={'green'} />
            </Animated.View>
            <Animated.View
              style={{
                opacity: this.nopeOpacity,

                transform: [{ rotate: "30deg" }],
                position: "absolute",
                top: 50,
                right: 40,
                zIndex: 1000
              }}
            >
              <MaterialIcon name={'mood-bad'} size={50} color={'red'} />
            </Animated.View>

            <View style={{ backgroundColor: '#fff', borderRadius: 10, flex: 1, padding: 10 }}>
              <Image
                style={{
                  flex: 0.8,
                  height: null,
                  width: null,
                  resizeMode: "cover",
                  borderTopLeftRadius: 10,
                  borderTopRightRadius: 10,
                }}
                source={{ uri: item.url }}
              />
              <View style={{ flex: 0.2 }}>
                <Text>{item.score}</Text>
                <Text>{item.title}</Text>
              </View>
            </View>
          </Animated.View>
        );
      } else {
        return (
          <Card key={i} item={item} index={i} />
        );
      }
    }).reverse();
  };

  render() {
    const { cards } = this.state
    return (
      <Query
      query={hostListQuery}
      variables={{
        name: 'aww',
        after: "t3_fela15"
      }}
    >
      {({ loading, error, data }) => {
        if (loading || error) return <Loading />
        return (
          this.context.cards = data.reddit.subreddit.hotListings.filter((d) => d.url.substr(-4) === '.jpg'),
          this.renderCards(data.reddit.subreddit.hotListings.filter((d) => d.url.substr(-4) === '.jpg'))
        )
      }}
    </Query>
    );
  }

}

mapStatetoProps = (state) => {
  return {
    allFavorites: state.favorites.allFavorites
  }
}

mapDispatchToProps = (dispatch) => {
  return {
    addFavorites: (item) => dispatch(addFavoriteAction(item)),
  }
}

export default connect(
  mapStatetoProps,
  mapDispatchToProps
)(CardStack)

const Styles = StyleSheet.create({
  cardContainer: {
    flex: 1,
    width: '100%',
  },
  cardStack: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
})