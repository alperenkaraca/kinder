import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

export const NoData = ({ iconName, text }) => {
    return (
        <View style={style.main}>
            <MaterialIcon name={iconName} size={25} color={'#000'} />
            <Text style={style.text}>{text}</Text>
        </View>
    )
}

const style = StyleSheet.create({
    main: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    text: {
        fontSize: 18, textAlign: 'center'
    }
})

