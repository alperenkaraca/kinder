import React from 'react'
import {View, ActivityIndicator, StyleSheet} from 'react-native'

export const Loading = () => {
    return(
        <View style={style.main}>
            <ActivityIndicator/>
        </View>
    )
}

const style=StyleSheet.create({
    main:{ flex:1, alignItems:'center', justifyContent:'center'}
})