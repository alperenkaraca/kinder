import React from 'react'
import { StyleSheet, Text, View, Dimensions, Image, TouchableOpacity } from 'react-native';

import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

const SCREEN_HEIGHT = Dimensions.get('window').height
const SCREEN_WIDTH = Dimensions.get('window').width

export const FavoriteCard = ({ item, deleteItem }) => {
    return (
        <View
            key={item.fullnameId}
            style={cardStyle.main}
        >
            <View style={cardStyle.contentContainer}>
                <Image
                    style={cardStyle.image}
                    source={{ uri: item.url }}
                />
                <View style={cardStyle.footer}>
                    <Text style={cardStyle.score}>{item.score}</Text>
                    <Text style={cardStyle.title}>{item.title}</Text>
                </View>
                <TouchableOpacity
                    onPress={() => deleteItem(item)} 
                    style={cardStyle.deleteButton}>
                    <MaterialIcon name={'delete'} size={25} color={'#fff'} />
                    <Text style={cardStyle.deleteButtonText}>Favoriler Cikar</Text>
                </TouchableOpacity>
            </View>


        </View>
    )
}

const cardStyle = StyleSheet.create({
    main: {
        height: SCREEN_HEIGHT - 220,
        width: SCREEN_WIDTH,
        padding: 20,

    },
    contentContainer: { backgroundColor: '#fff', borderRadius: 10, flex: 1, padding: 10 },
    image: {
        flex: 0.9,
        height: null,
        width: null,
        resizeMode: "cover",
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
    },
    footer: { flex: 0 },
    score: { color: '#9CD1CF', fontSize: 20, fontWeight: '800', marginVertical: 10, textAlign: 'right' },
    title: { color: '#5F8785', },
    deleteButton:{  backgroundColor: '#6D9190', flex: 0.1, marginVertical: 5, alignItems: 'center', justifyContent: 'center', flexDirection: 'row'  },
    deleteButtonText:{  color: '#fff', fontWeight: '800', fontSize: 18}

})