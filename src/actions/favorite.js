import { ADD_FAVORITE, REMOVE_FAVORITE, DELETE_ALL } from './constant'
const addFavoriteAction = (item) => {
    return {
        type: ADD_FAVORITE,
        item
    };
}

const removeFavoriteAction = (item) =>{
    return {
        type: REMOVE_FAVORITE,
        item
    };
}

const  deleteAllAction = () =>{
    return {
        type: DELETE_ALL,
    };
}

export {
    addFavoriteAction,
    removeFavoriteAction,
    deleteAllAction
}

