import { combineReducers } from 'redux'
import favoriteReducer from './favorite'
const rootReducer = combineReducers({
    favorites: favoriteReducer,
})

export default rootReducer