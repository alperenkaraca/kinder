
import { ADD_FAVORITE, REMOVE_FAVORITE, DELETE_ALL} from '../actions/constant'

const initialState = {
    allFavorites: [],
    newFavoriteItem: undefined,
    removedFavoriteItem:undefined
  
}

const remove = (arr, str) => {
    const elIdx = arr.findIndex((obj) => obj.fullnameId === str)
    return arr.filter((_, idx) => idx !== elIdx)
}

export default function favoriteReducer(state = initialState, action) {
    switch (action.type) {
        case ADD_FAVORITE:
            const newFavoriteItem = action.item;
            return {
                ...state,
                newFavoriteItem,
                allFavorites: [
                    ...state.allFavorites,
                    newFavoriteItem
                ]
            }
        case REMOVE_FAVORITE:
            const removedFavoriteItem = action.item;
            const removedArray = remove(state.allFavorites, removedFavoriteItem.fullnameId)
            return {
                ...state,
                removedFavoriteItem,
                allFavorites: removedArray
            }
        case DELETE_ALL:
            return {
                ...state,
                allFavorites: []
            }
        default:
            return state;
    }
}


