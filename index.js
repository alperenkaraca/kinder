/**
 * @format
 */
import React from 'react'
import {AppRegistry} from 'react-native';
import { Provider } from 'react-redux';
import configureStore from './configureStore'
import App from './App';
import {name as appName} from './app.json';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from '@apollo/react-hooks';
const client = new ApolloClient({
    uri: 'https://www.graphqlhub.com/graphql'
  });
const store = configureStore()
const AppRedux = () => (
    <ApolloProvider client={client}>
    <Provider store={store}>
        <App />
    </Provider>
    </ApolloProvider>
)

AppRegistry.registerComponent(appName, () => AppRedux);
